Format: 1.0
Source: brillant-bphox
Version: 0.84-1
Binary: brillant-bphox
Maintainer: Jerome Rocheteau <jerome.rocheteau@inrets.fr>
Architecture: any
Standards-Version: 3.6.0
Build-Depends: libc6, libncurses5, ocaml, ocaml-base, phox (>= 0.84-1), debhelper
Files: 
 6af00e4835e306243e59abf47378acae 1202460 brillant-bphox_0.84-1.tar.gz
