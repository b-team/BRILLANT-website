Format: 1.0
Source: brillant-core
Binary: brillant-core
Architecture: any
Version: 0.52
Maintainer: brillant-dev <brillant-dev@gna.org>
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Checksums-Sha1: 
 69d4bd142744b092d4e7e9d4d985d09b594891dd 11063 brillant-core_0.52.tar.gz
Checksums-Sha256: 
 61e4c2a5e7241cedf0b48abc22f56ebcfcd5e74c1ec1c71008bec5e17ea45b65 11063 brillant-core_0.52.tar.gz
Files: 
 c7d1f7313bff25c3e37d9fd2886abb04 11063 brillant-core_0.52.tar.gz
