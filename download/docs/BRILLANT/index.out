\BOOKMARK [1][-]{section.1}{Introduction}{}
\BOOKMARK [1][-]{section.2}{Overview of the BRILLANT platform}{}
\BOOKMARK [1][-]{section.3}{Case Study}{}
\BOOKMARK [1][-]{section.4}{The BCaml Kernel}{}
\BOOKMARK [2][-]{subsection.4.1}{BCaml input and output}{section.4}
\BOOKMARK [3][-]{subsubsection.4.1.1}{A concrete grammar for B}{subsection.4.1}
\BOOKMARK [3][-]{subsubsection.4.1.2}{Abstract syntax and XML syntax\204two isomorphic formats}{subsection.4.1}
\BOOKMARK [3][-]{subsubsection.4.1.3}{How BCaml exploits the Abstract Syntax Tree \(AST\)}{subsection.4.1}
\BOOKMARK [2][-]{subsection.4.2}{B modularity related AST processing}{section.4}
\BOOKMARK [3][-]{subsubsection.4.2.1}{The flattening algorithm}{subsection.4.2}
\BOOKMARK [3][-]{subsubsection.4.2.2}{The B-HLL module system}{subsection.4.2}
\BOOKMARK [3][-]{subsubsection.4.2.3}{ Code generation feedback }{subsection.4.2}
\BOOKMARK [2][-]{subsection.4.3}{Generating proof obligations}{section.4}
\BOOKMARK [3][-]{subsubsection.4.3.1}{Generalized Substitution Language \(GSL\)}{subsection.4.3}
\BOOKMARK [3][-]{subsubsection.4.3.2}{Proof Obligation Generation}{subsection.4.3}
\BOOKMARK [3][-]{subsubsection.4.3.3}{Exporting to other tools}{subsection.4.3}
\BOOKMARK [1][-]{section.5}{From B proof obligations to correctness}{}
\BOOKMARK [2][-]{subsection.5.1}{Coq}{section.5}
\BOOKMARK [2][-]{subsection.5.2}{Implementation of the mathematical foundations of B}{section.5}
\BOOKMARK [2][-]{subsection.5.3}{Theoretical and experimental results}{section.5}
\BOOKMARK [3][-]{subsubsection.5.3.1}{Theoretical results}{subsection.5.3}
\BOOKMARK [3][-]{subsubsection.5.3.2}{Experimental results}{subsection.5.3}
\BOOKMARK [2][-]{subsection.5.4}{Closing thoughts about BiCoax}{section.5}
\BOOKMARK [1][-]{section.6}{From B specifications to code}{}
\BOOKMARK [1][-]{section.7}{From UML/OCL models to B specifications}{}
\BOOKMARK [1][-]{section.8}{Discussion, conclusion and perspectives}{}
\BOOKMARK [2][-]{subsection.8.1}{Comparison with other works}{section.8}
\BOOKMARK [2][-]{subsection.8.2}{Conclusion}{section.8}
\BOOKMARK [2][-]{subsection.8.3}{Perspectives}{section.8}
