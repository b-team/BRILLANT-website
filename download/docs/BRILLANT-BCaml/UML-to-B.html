<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
            "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="hevea 1.09">
<LINK rel="stylesheet" type="text/css" href="draft.css">
<TITLE>From UML/OCL models to B specifications</TITLE>
</HEAD>
<BODY >
<A HREF="code-generation.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="conclusion.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
<HR>
<H2 CLASS="section"><A NAME="htoc22">7</A>  From UML/OCL models to B specifications</H2><UL>
<LI><A HREF="UML-to-B.html#toc7">From requirements to UML models</A></LI>
<LI><A HREF="UML-to-B.html#toc8">UML-based-modelling</A></LI>
<LI><A HREF="UML-to-B.html#toc9">Adding OCL constraints</A></LI>
<LI><A HREF="UML-to-B.html#toc10">Formalization of classes and state diagrams</A></LI>
<LI><A HREF="UML-to-B.html#toc11">Formalization of OCL constraints</A></LI>
<LI><A HREF="UML-to-B.html#toc12">Verification of the entire model</A></LI>
</UL>
<P>
<A NAME="sec:from-umlocl-models-to-b"></A></P><P>This section introduces an UML plug-in for BCaml that helps to verify the 
consistency of an UML model by translating it into a B specification and 
using the verification tools validate the B specification, 
which in turn ensures the consistency of the UML model. 
Sections <A HREF="#sec:from-reqs-to-uml">7.1</A>,<A HREF="#sec:uml-based-modelling">7.2</A>
and <A HREF="#sec:adding-ocl-constraints">7.3</A> presents the steps involved
in specifying a UML model and the associated safety constraints. 
The different steps are illustrated using the example of the railway 
level-crossing example presented in section <A HREF="case-study.html#sec:casestudy">3</A>. 
Sections <A HREF="#sec:formalisation-classes-state-diagrams">7.4</A>, 
<A HREF="#sec:form-ocl-constraints">7.5</A>, and <A HREF="#sec:verification-entire-model">7.6</A>, 
respectively, describe how to translate UML diagrams into B, how to enrich
the obtained B specification with the UML model's OCL constraints, 
and how to prove the consistency of the UML model by verifying the 
enriched B specification. </P><H3 CLASS="subsection"><A NAME="toc7"></A><A NAME="htoc23">7.1</A>  From requirements to UML models</H3><P>
<A NAME="sec:from-reqs-to-uml"></A></P><P>The "elicitation problem" (i.e., the creation of a valid initial
description of the required system properties) is essential to ensure
the correctness of the future system. The consistency of the system
depends on the developer's ability to understand and incorporate key
safety properties. Therefore, knowledge of the context in which the
system operates plays an important role in eliciting system
requirements. Our approach to requirement analysis is based on the
approach taken by Marcano <I>et al.</I> [<A HREF="draft010.html#FORMS04-Marcano"><CITE>36</CITE></A>], in
which both the static and dynamic properties of the system are taken
into account through various UML diagrams.</P><P>Describing the entities involved and their invariants facilitates
comprehension of the static properties. Describing the way that
system actors will interact with each other and the system leads to a
full comprehension of the dynamic properties of the future system. The
Object Constraint Language (OCL) is used to express all the properties
that cannot be expressed through diagrammatic notation alone (i.e.,
hypotheses and facts related to subsystems, classes, attributes and
associations). OCL is also used to describe the system safety
conditions. Thus, system modeling can be divided into two steps:
</P><UL CLASS="itemize"><LI CLASS="li-itemize">
First, UML sequence diagrams must be defined in order to
describe both correct operating scenarios and failure scenarios
</LI><LI CLASS="li-itemize">Second, the expected behavior of the system must be described
completely as a set of OCL pre- and post-operational conditions
</LI></UL><P>
UML state diagrams will be defined for the combined operations of the
entire system in order to describe the overall interaction of the
system with each actor in its environment.</P><H3 CLASS="subsection"><A NAME="toc8"></A><A NAME="htoc24">7.2</A>  UML-based-modelling</H3><P>
<A NAME="sec:uml-based-modelling"></A></P><P>In the railway example described in section <A HREF="case-study.html#sec:casestudy">3</A>, 
the central system is the level-crossing control (LCC) system, 
the two others-a train-borne control (TC) system and an operations 
center (OC) system-being cooperating actors that make use of the LCC. 
To create a UML model, first, it is necessary to identify the main entities
that must be
modelled to determine possible LCC system failure conditions. A
primary cause of such failure conditions could be malfunctioning
sensors or actuators. Defects leading to failures may be detected in
the main physical structures or in the control systems themselves. In
this case study, only a limited number of failures are considered:
failures of the yellow or red traffic lights (which are considered
separately), the barriers, and the vehicle sensor, and the delay or loss
of messages sent through the radio network. For any of these failures, the
following objects that interact with the LCC system
(Fig.<A HREF="#classdiag1">3</A>) are examined: the lights, the barriers, the
vehicle sensors, the train-borne control system, and the operations
center.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="draft002.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 3: RLC system : Class diagram</TD></TR>
</TABLE></DIV>
<A NAME="classdiag1"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><H3 CLASS="subsection"><A NAME="toc9"></A><A NAME="htoc25">7.3</A>  Adding OCL constraints</H3><P>
<A NAME="sec:adding-ocl-constraints"></A></P><P>Using a standard formal language for constraint specification is an
important step towards formalizing complex models, particularly in the
context of critical safety systems. The purpose of OCL is to allow the
constraints related to system objects to be formally specified,
preserving the comprehensibility and readability of the UML models.
OCL facilitates the statement of the properties and the invariants of
the objects, as well as that of the pre/post-conditions for the
operations. OCL also provides a navigation mechanism that allows
attributes, operations and associations to be referenced in the
context of a class or an object (a class variable), and query
operators that permit a set of elements to be selected and/or
modified. Each OCL expression has a specific type and belongs to a
specific context. The context of an OCL expression determines its
scope. Only the visible elements in the context of the expression can
be referenced by means of navigation expressions.</P><P>Safety properties are included in the system invariants in order to
propagate them from the abstract specification phase to the
implementation phase. The main property of the LCC system is to
prevent both road and rail traffic from entering the danger zone at
the same time; to do so, the control specifications for the crossing
area and its barrier, as well as any trains that may pass through the
level crossing at any time, must be modelled at a high level of
abstraction. For these reasons, the following OCL invariants are
specified for these classes, as shown in Fig.<A HREF="#classdiag0">4</A>:</P><OL CLASS="enumerate" type=1><LI CLASS="li-enumerate">
<B>Req.</B><I><FONT COLOR=maroon>If there is a train crossing the danger zone then the
barrier is closed</FONT></I>

<BR>

<FONT COLOR=purple>
context CrossingArea inv:<BR>
 not(self.train-&gt;isEmpty()) implies self.barrier.state=Closed 
<BR>

</FONT></LI><LI CLASS="li-enumerate"><B>Req.</B><I><FONT COLOR=maroon>If the barrier of the crossing area is open, then no
train is approaching the danger zone</FONT></I>

<BR>

<FONT COLOR=purple>
context Barrier inv:<BR>
 self.state=Opened implies self.guards.train-&gt;isEmpty()
<BR>

</FONT></LI><LI CLASS="li-enumerate"><B>Req.</B><I><FONT COLOR=maroon>If the barrier of the crossing area is closed, then a train is 
crossing the intersection</FONT></I>

<BR>

<FONT COLOR=purple>
context PhysicalTrain inv:<BR>
 self.crosses.barrier=Closed 
<BR>

</FONT>
</LI></OL><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="draft003.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 4: Constraints related to the danger zone</TD></TR>
</TABLE></DIV>
<A NAME="classdiag0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>These constraints must hold true for a more detailed design once
decisions have been made about the actual type of hardware to be used
in an implementation. In this case study, the notion of "train passing
through the intersection" is connected to the activation of the
railway level crossing. In order to accomplish this task, the front
and the rear of the train must somehow be detected. We assume that
the train can be detected directly through use of abstract vehicle
sensors. The barrier state is detected by introducing a barrier
sensor. In light of these assumptions, the previous OCL invariants
can be refined by adding the following LCC system constraints for the
class shown in Fig.<A HREF="#classdiag1">3</A>:</P><OL CLASS="enumerate" type=1><LI CLASS="li-enumerate">
<B>Req.</B><I><FONT COLOR=maroon>The red light is switched on whenever the barrier
is closed, and the yellow light is switched on when the barrier is
closing. If both the yellow and the red lights are switched off,
then the barrier is open.</FONT></I>
<P><BR>

<FONT COLOR=purple>
context LCC_System inv:<BR>
 self.theBarrier.state=Closed implies self.redLight.state=On<BR>
 and self.theBarrier.state=Closing implies self.yellowLight.state=On<BR>
 and self.yellowLight.state=Off and self.redLight.state=Off<BR>
 implies self.theBarrier.state=Opened
<BR>

</FONT></P></LI><LI CLASS="li-enumerate"><B>Req.</B><I><FONT COLOR=maroon>If a train is in the danger zone, the level
crossing is in an activated state composed of four substates
(WaitingAck, Closing, Closed, Opening).</FONT></I>
<P><BR>

<FONT COLOR=purple>
context LCC_System inv:<BR>
 not(self.train-&gt;isEmpty()) implies self.state=Activated and<BR>
 
Set(Activated)=Set(WaitingAck-&gt;Union(Closing)-&gt;Union(Closed)-&gt;Union(Opening))
<BR>

</FONT></P></LI><LI CLASS="li-enumerate"><B>Req.</B><I><FONT COLOR=maroon> If the LCC system is in the activated state while
the barrier is open, then the level crossing is in an unsafe mode.</FONT></I>
<P><BR>

<FONT COLOR=purple>
context LCC_System inv:<BR>
 self.state=Activated and self.bSensor.state=Opened implies
self.mode=Unsafe 
<BR>

</FONT></P></LI><LI CLASS="li-enumerate"><B>Req.</B><I><FONT COLOR=maroon>If the registered state of the barrier is closed
and the trigger sensor indicates that it is open, then the level
crossing is in an unsafe mode. This is the case when the barrier
is in the closing state; the LCC remains unsafe until the barrier
is completely closed. </FONT></I>
<P><BR>

<FONT COLOR=purple>
context LCC_System inv:<BR>
 self.bSensor.state=Opened and self.theBarrier.state=Closed)
implies self.mode=Unsafe 
<BR>

</FONT></P></LI></OL><P>The operations of the LCC class are specified with OCL pre- and
post-conditions. OCL is also used in sequence diagrams to complete
the preconditions and invariants related to operations. Although
state diagrams are used to derive the initial specification of each
operation (i.e., the description of a state transition), OCL
constraints are needed to add supplementary information that can not
be retrieved from the state diagrams.</P><P>Consider the closing of the barrier raised by the event
<TT>timeOut_1</TT>. The precondition of the operation
<TT>closeBarrier</TT> ensures that the yellow light is switched on
before sending the closeBarrier order, in addition to ensuring that
the barrier has not yet been closed. The postcondition ensures that
the state of the yellow light is off, the state of the red light is
on, and the state of the barrier is closed. The operation is specified
as follows:</P><P><BR>

<FONT COLOR=purple>
context LCC_System::closeBarrier<BR>
 pre: self.yellowLight.state=On and self.theBarrier.state=Opened<BR>
 post: self.yellowLight.state=Off and self.redLight.state=On and<BR>
 self.theBarrier.state=Closed 
<BR>

</FONT></P><H3 CLASS="subsection"><A NAME="toc10"></A><A NAME="htoc26">7.4</A>  Formalization of classes and state diagrams</H3><P>
<A NAME="sec:formalisation-classes-state-diagrams"></A></P><P>Our main goal is to extract an initial B specification (called the
&#X201C;abstract&#X201D; specification) from the UML diagrams and to use it to
check for inconsistencies. To do so, an abstract machine is associated
to each class. Subsequently, the B method is used to provide details
about each component with regard to the behavior of class operations
and the global invariants. At this point, the system developer must
make several important decisions concerning unspecified properties and
then introduce these properties into the UML diagrams using OCL
constraints. The UML diagrams are then translated into B, and the
resulting B specification is used to check the consistency of all the
UML diagrams and OCL constraints. </P><P>Consequently, the B specification is not a good illustration of 
code generation because the overall process was designed with
consistency checking in mind rather than code generation.
However, the existence of a tool allows us to explore in what 
measure code generation is feasible, and what adaptations to the process need to be made in order to be able to generate code from UML specifications.</P><H4 CLASS="subsubsection"><A NAME="htoc27"></A>Classes</H4><H5 CLASS="paragraph"> </H5><P>Consider the class <I><FONT COLOR=maroon>Barrier</FONT></I>
 and its first B specification,
presented in Fig. <A HREF="#class">5</A>. Since a class includes both the static
and dynamic properties of a set of objects, it seems natural to model
it using one abstract machine. The resulting abstract machine Barrier
describes the deferred set BARRIER of all the possible
instances of the class <I><FONT COLOR=maroon>Barrier</FONT></I>
. The set of existing
instances is modelled using a variable barrier constrained to be a
subset of BARRIER.</P><P>Each attribute (i.e., <I>bState</I>) is represented by a variable
(i.e., bState) defined in the INVARIANT clause as a total
function between the set barrier and its associated type (i.e.,
bSTATE). Each operation of the machine has at least one parameter obj
representing the object on which the operation is called. It may have
a list of typed arguments args, which will be completed in the later
translation of the state diagrams and OCL constraints.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]Barrier1.amn
</DIV>
<DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]Barrier2.amn
</DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 5: Formalization of classes (machine Barrier)</TD></TR>
</TABLE></DIV>
<A NAME="class"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><H4 CLASS="subsubsection"><A NAME="htoc28"></A>Formalization of state diagrams</H4><H5 CLASS="paragraph"> </H5><P>State diagrams are used to introduce the behavioral (i.e., dynamic)
properties of the system into the B specification. The set of all
possible states of a class is formalized using an abstract set that is
defined in the corresponding B machine. An abstract variable is used
to reference the current state of the class objects. This variable is
defined as a total function, whose domain is the set of instances and
whose range is the set of possible states. Each transition between two
states is formalized by a B operation, whose name is that of the
incoming event. Whereas the precondition of the operation is deduced
from the transition guard, the postcondition describes the transition
to the new state. Let us consider the state diagram of the
<TT>LCC_System</TT> class shown in figure <A >??</A>: the
transition from the <TT>showingYlight</TT> state to the <TT>closingB</TT>
state activated by the event <TT>timeOut_1</TT> is formalized as shown
in Fig.<A HREF="#transition0">6</A>. Note that we have included some information
obtained from the OCL definition of the operation closeBarrier, since
this operation is activated by the event <TT>timeOut_1</TT>. (The OCL
translation is described below.)</P><P>When the same event can activate two different transitions depending
on the guard condition, then both transitions are formalized by the
same operation of the B machine. The non-deterministic construction
<SPAN STYLE="font-variant:small-caps">select</SPAN> is used to describe each transition, as illustrated in
Fig.<A HREF="#transition0">6</A> for the formalization of the event
<TT>timeOut_2</TT>. 
The time constraints (not shown in the example, see reference [<A HREF="draft010.html#Marcano04"><CITE>37</CITE></A>]
for details) are handled in the precondition by checking the value of
a clock variable defined in a abstract clock machine. The progression of time is
added to the body of the operations, by calling the relevant operation
of the clock machine with a value of time progress. This value can be
made non-deterministic by using the &#X201C;unbounded choice&#X201D; construct of
B.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]LCC_System1.amn
</DIV>
<DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]LCC_System2.amn
</DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 6: Formalization of state diagrams</TD></TR>
</TABLE></DIV>
<A NAME="transition0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>Once the classes and state diagrams have been translated and
integrated into the initial specification, OCL constraints are used to
complete the B machine invariants and operations.</P><H3 CLASS="subsection"><A NAME="toc11"></A><A NAME="htoc29">7.5</A>  Formalization of OCL constraints</H3><P>
<A NAME="sec:form-ocl-constraints"></A></P><P>In this section, we explain how OCL expressions are translated into B expressions, using the rules defined for the OCL
meta-model in Marcano's PhD dissertation [<A HREF="draft010.html#Marcano-Phd"><CITE>28</CITE></A>].</P><P>The Object Constraint Language has thus far been defined semi-formally
using textual descriptions, a grammar that specifies the concrete
syntax, and examples that illustrate the semantics of the expressions.
Such a presentation style is adequate for illustrating OCL concepts,
but it is not sufficient for providing a rigourous semantic. This
semi-formal nature of the OCL definition, which often leads users to
interpret the UML models ambiguously, restricts its use in critical
safety applications. This difficulty is increased by the lack of
tools supporting the analysis of OCL expressions and the "proof" of
complete UML models.</P><P>Recent work proposing a precise semantic for OCL has been carried out
by Richters and Gogolla [<A HREF="draft010.html#Richters98b"><CITE>49</CITE></A>]. In addition, both these
authors [<A HREF="draft010.html#Richters00"><CITE>50</CITE></A>] and Jackson <I>et al.</I> [<A HREF="draft010.html#Jackson00"><CITE>26</CITE></A>]
have published research related to tools for verifying UML designs.
The first publication proposes an approach for validating UML models
using simulation, and the second proposes an object model analyzer
that uses Alloy, which is based on Z. Two of the authors of the
present paper have also previously worked to formalize OCL with B,
using a system of translation rules between the abstract syntaxes of
both languages [<A HREF="draft010.html#Marcano02b"><CITE>34</CITE></A>].</P><P>In the <EM>BRILLANT</EM> plug-in, two types of OCL constraints are taken into
account. The first type of constraint specifies an invariant of a
class, and the second type specifies a precondition and/or a
postcondition of an operation. In the first case, translating the OCL
constraint consists of combining a new predicate with the invariant of
the related B machine, whereas in the second case, it requires
completing an operation of the machine. The formalization of the LCC
system's OCL invariant is shown in Fig.<A HREF="#invariant">7</A>.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center">
<DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]LCC_System-OCL.amn
</DIV>
</DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 7: Formalization of OCL invariants</TD></TR>
</TABLE></DIV>
<A NAME="invariant"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>In the same way that OCL predicates enrich the UML model, OCL pre- and
post-conditions are used to enrich B machine operations. In
Fig.<A HREF="#transition0">6</A>, the pre-condition of the operation
<TT>timeOut_1</TT> not only requires that the LCC system be in the
<TT>yellowLight</TT> state (which is generated from the state diagram)
but also that the red light be switched on and the barrier be closed.
These three contraints together constitute the translation
of the OCL predicate: </P><P><BR>

<FONT COLOR=purple>
self.yellowLight.state=On and self.theBarrier.state=Opened.
<BR>

</FONT></P><P>The post-condition of the operation initially includes only the substitution 
<TT>state(obj):=ClosingB</TT>
that sets the new state of the LCC instance (obj). This
post-condition is completed by translating the OCL post-condition</P><P><BR>

<FONT COLOR=purple>
self.yellowLight.state=Off and
self.redLight.state=On and self.theBarrier.state=Closed 
<BR>

</FONT>
into B, which generates the following parallel substitutions: </P><P><TT>closeBarrier (lcc_barrier(obj))<BR>
|| Yellow.switchOff
(yellowLight(obj))<BR>
|| Red.switchOn (redLight(obj)) </TT></P><P>Please note that although OCL constraints can be sufficient for
expressing the behavior of the model, it can be very difficult to
extract this behavior in order to express it in terms of B
operations. Thus, state diagrams are necessary to provide the
skeleton for the behavior, upon which additional information can be
imported from the OCL pre- and post-conditions.
</P><H3 CLASS="subsection"><A NAME="toc12"></A><A NAME="htoc30">7.6</A>  Verification of the entire model</H3><P>
<A NAME="sec:verification-entire-model"></A></P><P>In order to automate the formalization process, we implemented a
prototype tool that derives B specifications from UML/OCL models.
The strength of this tool is that it does not depend on any UML
modelling tool. Instead, it does the translation using XMI files
(i.e., files in an XML format describing a UML architecture) as
input. As a result, this tool can still be used even if UML modelling tools
change their Application Programming Interface.</P><P>Once the whole B formal specification has been generated from the
UML/OCL model, it has to be type-checked and then verified through a
proof process. A dedicated tool is then used to automatically
generate and prove the proof obligations (POs). The POs guarantee that
the B machine's operations conform to its invariant. Each operation
raises proof obligations related to its pre-condition and substitution
parts. The non-proven POs are used to detect inconsistencies between
the invariant and the preconditions, as well as to detect the
incompleteness of a post-condition.</P><P>If a proof obligation cannot be proven using the theorem prover, then
the developer must review the related OCL invariant or operation and
make the necessary modifications to allow the obligation to be proved.
Our approach is a one-way approach: it allows UML/OCL to be translated
into B, but not the other way round. When the type-checker or the
prover finds an error in the specification, the user must first
understand the B specification and then search in the UML/OCL model
to find the error. Please note that it is quite simple for the
developer to find the UML element associated to a B expression
because the names are roughly the same and each OCL expression is
translated into a simple B expression. Thus, in order to facilitate
the task, we have made it possible to create and maintain concrete
links between the UML/OCL models and B specifications throughout the
development process.</P><HR>
<A HREF="code-generation.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="conclusion.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
</BODY>
</HTML>
