<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
            "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="hevea 1.09">
<LINK rel="stylesheet" type="text/css" href="draft.css">
<TITLE>The BCaml Kernel</TITLE>
</HEAD>
<BODY >
<A HREF="case-study.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="correctness.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
<HR>
<H2 CLASS="section"><A NAME="htoc4">4</A>  The BCaml Kernel</H2><UL>
<LI><A HREF="bcaml-kernel.html#toc1">BCaml input and output</A></LI>
<LI><A HREF="bcaml-kernel.html#toc2">B modularity related AST processing</A></LI>
<LI><A HREF="bcaml-kernel.html#toc3">Generating proof obligations</A></LI>
</UL>
<P>
<A NAME="sec:from-b-spec-to-xml"></A></P><P>In this section, we describe the three main component parts of <EM>BCaml</EM>: a
parser (with an XML output library to connect <EM>BCaml</EM> with the outside
XML world), libraries to handle the modularity of B projects, and a proof
obligation generator.</P><H3 CLASS="subsection"><A NAME="toc1"></A><A NAME="htoc5">4.1</A>  BCaml input and output</H3><P>
<A NAME="sec:kernel-bcaml-input-output"></A></P><P>
The kernel of the <EM>BCaml</EM> platform is made up of the first bricks that
were developed when the platform was created. These bricks specify
the concrete grammar (section <A HREF="#sec:grammar-b">4.1</A>) that defines the B language and the abstract syntax
(section <A HREF="#sec:abstract-syntax-xml">4.1</A>) that defines the type used to
manipulate the specifications. This may seem obvious, but it is
nonetheless important because it makes collaboration with other
developers possible. One of these collaborative projects led to the
development of another brick in the <EM>BCaml</EM> kernel, called the
<EM>Btyper</EM>. This brick is not described in this paper, but more
details can be found in Bodeveix &amp; Filali [<A HREF="draft010.html#ZB02-Bodeveix"><CITE>9</CITE></A>].</P><H4 CLASS="subsubsection"><A NAME="htoc6"></A>A concrete grammar for B </H4><P>
<A NAME="sec:grammar-b"></A>
</P><H5 CLASS="paragraph"> </H5><P>Several B grammars have been introduced over the years, coming from a
variety of sources: <EM>Clearsy</EM> (prev. <EM>Steria</EM>), which
corresponds to the grammar used in <EM>Atelier B</EM>; <EM>B-Core</EM>,
which corresponds to the grammar used in <EM>B-Toolkit</EM>; and
Mariano's PhD dissertation [<A HREF="draft010.html#Mariano-Phd"><CITE>38</CITE></A>], based on the
<EM>B-Core grammar</EM>, which was introduced to do metrics on B specifications.</P><P>In order to build a tool that would be as useful as possible, we
needed to define a grammar that would take into account the criticisms
of the grammars presented above. Our <EM>BCaml</EM> choices had to respect the
following constraints:
</P><UL CLASS="itemize"><LI CLASS="li-itemize">
They had to be as compatible as possible with the machines that
can be correctly parsed by the commercial tools mentioned above
(i.e., <EM>Atelier B</EM>, <EM>B-Toolkit</EM>),
</LI><LI CLASS="li-itemize">they had to comply with the standard <EM>Lex</EM> and <EM>Yacc</EM>
tools that allow <EM>LALR</EM> grammars to be defined, and
</LI><LI CLASS="li-itemize">they had to cause as few conflicts as possible.
</LI></UL><P>We chose OCaml [<A HREF="draft010.html#ocaml309"><CITE>31</CITE></A>] as a support tool for our B development for several reasons. Not only does it allow symbolic
notations to be handled easily, but in addition, it also implements efficiently
and comes bundled with tools that allow the
parsing of <EM>LALR</EM> grammars. Using these tools, we defined the B language in <EM>LALR</EM>. </P><H4 CLASS="subsubsection"><A NAME="htoc7"></A>Abstract syntax and XML syntax&#X2014;two isomorphic formats</H4><P>
<A NAME="sec:abstract-syntax-xml"></A>
</P><H5 CLASS="paragraph"> </H5><P>We used our definition of abstract syntax to directly infer an XML
representation for B formal specifications. (Due to lack of space,
this abstract syntax is not described here.) This XML encoding is
called "B/XML" and is stored in an XML DTD file. Such abstract syntax
is, as could be expected, more tolerant than concrete syntax, and
contains elements that facilitate the handling of the syntax
structure. For instance, the [<I>substitution</I>]<I>predicate</I> and
[<I>variable</I>_<I>instanciation</I>]<I>substitution</I> constructions appear in this
abstract syntax. The first construction corresponds to the form of a
formula prior to a calculation of its weakest precondition. The second
construction appears, for instance, when the parameters and
operations have to be instanciated. The existence of these
constructions in the abstract syntax mean that the structure can be
manipulated to bring it closer to the matching mathematical
definitions given in the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A>].</P><P>We chose XML as our pivot format because of its flexibility and its
ease-of-use with third-party tools. Using it makes our tools as
independent of one another as possible, allowing a researcher to use
our parser, but someone else's proof tool, for example. This
flexibility is insured by the following aspects:
</P><UL CLASS="itemize"><LI CLASS="li-itemize">
XSL style sheets can be used to formulate simple recursive
treatments of the XML structure, mostly transformations into other
structured formats (e.g., L<sup>A</sup>T<sub>E</sub>X, HTML, or PhoX, as mentioned in
section <A HREF="#sec:exporting-to-other-tools">4.3</A>); and
</LI><LI CLASS="li-itemize">other programming languages can
be easily used for more complex manipulations because most of the time 
these other languages are able to read XML data. 
This means that researchers can use their preferred programming
language, as long as it has libraries for reading an XML format.
</LI></UL><H4 CLASS="subsubsection"><A NAME="htoc8"></A>How <EM>BCaml</EM> exploits the Abstract Syntax Tree (AST)</H4><P>
<A NAME="sec:how-bcaml-exploits-ast"></A>
</P><H5 CLASS="paragraph"> </H5><P><EM>BCaml</EM> takes advantage of both of the features described above.
The XML approach extends the platform's plug-in/plug-out ability greatly, 
while the use of a well-defined, efficient meta-language as the core 
implementation language leads to a formal standard definition of the B grammar 
and allows the provision of more efficient components, easily understandable 
by others. 
The main drawback of this choice of formats is the difficulty of making both
formats evolve together. When designing a component interface to be plugged in,
one natural guideline is to use the core implementation language if high 
efficiency is required, in order to avoid a translation step 
(which is the choice made for the <EM>BTyper</EM>, the <EM>POG</EM> and the AST
manipulation tools, for example), and to rely on the XML exchange format
in all other cases (which is the alternative chosen for B/PhoX). </P><H3 CLASS="subsection"><A NAME="toc2"></A><A NAME="htoc9">4.2</A>  B modularity related AST processing</H3><P><A NAME="sec:kernel-astmanipulation"></A>
</P><P>Two examples of the complex AST manipulations available in the <EM>BRILLANT</EM> platform are presented in the following sub-sections: </P><DL CLASS="description"><DT CLASS="dt-description">
<B>Flattening,</B></DT><DD CLASS="dd-description"> which can be seen as a way of expanding the abstract
mathematical code of a machine into a concrete code through its
refinements and included machines;
</DD><DT CLASS="dt-description"><B>Modularisation,</B></DT><DD CLASS="dd-description"> which involves using a well-grounded modular system that produces a 
modular language from a flat language and a description of the
desired modularity.
</DD></DL><H4 CLASS="subsubsection"><A NAME="htoc10"></A>The flattening algorithm</H4><P>
<A NAME="sec:depliage"></A>
</P><H5 CLASS="paragraph"> </H5><P>B specifications are flattened by eliminating the refinement and
composition links. The flattening algorithm aims
to build a single B component, starting with a set of B components (a B "model") 
and grouping all the (selected) information extracted from the various
specifications grouped into one formal text. </P><P>The resulting component is the "equivalent" of the initial model from the point 
of view of automatic code generation.</P><P>This notion of flattening exists implicitly in the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A>]. 
Though Potet and Rouzaud used the term "flattening" in their work
[<A HREF="draft010.html#B98-Potet-nocross"><CITE>47</CITE></A>], it was Behnia [<A HREF="draft010.html#Behnia-Phd"><CITE>6</CITE></A>] who
specified the algorithm entirely, and it's her specification that we
used in our tool. The principle of the algorithm is to connect the
specification from the leaves (where only the <TT>IMPORTS</TT> and
<TT>REFINES</TT> links are taken into account) to the root machine of the
project.</P><P>
<EM>Implementation</EM>: The flattening tool was the first tool
implemented after the <EM>BCaml</EM> kernel was designed
(section <A HREF="#sec:from-b-spec-to-xml">4</A>). 
This implementation was intended to "evaluate" the kernel's usability and to add
those tools/libraries that would be useful for
manipulating B specifications to the platform.
In order to implement the tool, two things had
to be done. First, the specification dependency graph had to be
equipped to
navigate through the specifications in order to build the successive
flattened components. 
To accomplish this, we developed a library called
BGraph, which implements the dependency graph type and the functions
needed to manipulate that graph. Second, all the conditions that
allow a set of B components to be flattened had to be verified.
</P><H4 CLASS="subsubsection"><A NAME="htoc11"></A>The B-HLL module system</H4><P>
<A NAME="sec:bhll"></A></P><H5 CLASS="paragraph">Overview</H5><P>The Harper-Lillibridge-Leroy module system (HLL) presented in Leroy
[<A HREF="draft010.html#Leroy-modular-modules-jfp"><CITE>32</CITE></A>] formalizes the Standard ML-like
modules. The HLL system provides a means for adding a module language
to a module-less core language. This system also permits a formal
semantic to be given to an existing module language, as is the case
for the ML modules. Moreover, this powerful semantic is able to
implement the module language with relative simplicity.</P><P>Once the HLL module system has been instantiated, it is possible to
define structures (i.e., list of values, types, modules or sub-modules)
and functors (module-to-module functions) in the obtained modular
language. A more complete description of our work on B-HLL can be
found in our article published in 2004 [<A HREF="draft010.html#petit-fdl04"><CITE>44</CITE></A>].</P><H5 CLASS="paragraph">Instantiation</H5><P>Figure <A HREF="#fig:hll">2</A> sums up our use of the HLL system. 
As shown in the middle level in figure, the HLL
module system includes three functors. 
These functors are given modules defining the abstract
grammar and the type-checking rules (upper level of the figure) and
in turn produce several modules (lower level of the figure)
that deal with the modularity of the language.</P><P>Our efforts to instantiate the HLL module system were divided into 
two parts, which are shown on the upper level of Figure <A HREF="#fig:hll">2</A>.
The first part
involved defining the abstract language of the B core language under
study, based on the abstract syntax defined during the development of
the <EM>BCaml</EM> kernel. From this abstract syntax, we removed the part of
the syntax dedicated to the modularity language, and then we developed
a mapping function from the <EM>BCaml</EM> kernel abstract syntax to our new
abstract syntax.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
file=figures/utilisationHLL-uk.eps,
width=0.9
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 2: Using the HLL module system</TD></TR>
</TABLE></DIV>
<A NAME="fig:hll"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></DIV></BLOCKQUOTE><P>
The second part of the instantiation involved defining the type
checker. The types and the type-checking algorithm we used were
adapted from the work of Bodeveix and Filali
[<A HREF="draft010.html#ZB02-Bodeveix"><CITE>9</CITE></A>]. We added some type-checking rules to express
the visibility rules described in the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A>], and we
also defined type-checking rules that take into account B language
particularities, such as the semi-hiding principle and the
prohibition of calling a given operation in the component where that
operation is defined. (More details about this can be found in
[<A HREF="draft010.html#petit-fdl04"><CITE>44</CITE></A><CITE>, </CITE><A HREF="draft010.html#PetitPhd"><CITE>42</CITE></A>]).

</P><P>The modules produced by the HLL system are:
</P><DL CLASS="description"><DT CLASS="dt-description">
<B>Amod:</B></DT><DD CLASS="dd-description"> the module for modularity functions, indicating what kinds of
modules are available;
</DD><DT CLASS="dt-description"><B>Aenv:</B></DT><DD CLASS="dd-description"> the module for environment handling, indicating how information can
be retrieved from referenced modules;
</DD><DT CLASS="dt-description"><B>AModTyping:</B></DT><DD CLASS="dd-description"> the module for modular typechecking, indicating how types 
can be inferred in a modular context.
</DD></DL><P>By using the HLL system, it is possible to build a modular language for B almost 
automatically, without knowing much about the internals of the HLL system. </P><H4 CLASS="subsubsection"><A NAME="htoc12"></A> Code generation feedback </H4><P>
<A NAME="sec:Bmod"></A>
</P><H5 CLASS="paragraph"> </H5><P>Although implementing the flattening algorithm validated the already 
developed bricks and constituted our first complete code production
chain, using a published formally-defined system like the HLL module
system as the core of the code generation process had an unexpected
benefit in that it clarified the notion of modularity in the B language.
Though the B developers do not need to know the details of how modularity
is implemented, they do need to know that the generation tool complies
with all the visibility rules specified in the B-Book. 
This tool allows structured code to be generated in components
equipped with contracts (see section <A HREF="code-generation.html#sec:from-b-spec-to-code">6</A>). </P><H3 CLASS="subsection"><A NAME="toc3"></A><A NAME="htoc13">4.3</A>  Generating proof obligations</H3><P>
<A NAME="sec:bcaml-pog"></A></P><P>In this section, we first describe the method used to implement the
existing calculus for the weakest precondition. Then, we show how
this calculus can be used to generate the proof obligations of a B
project. Last, we present the various options available for exporting
these proof obligations to other formats and other tools.</P><H4 CLASS="subsubsection"><A NAME="htoc14"></A>Generalized Substitution Language (GSL)</H4><P>
<A NAME="sec:GSL"></A>
</P><H5 CLASS="paragraph"> </H5><P>In order to generate proof obligations for B machines, we must be
able to calculate the weakest preconditions of the substitutions. In a
nutshell, the weakest precondition calculus allows the minimal state
for a given program, or <EM>substitution</EM>, to be calculated in order
to verify a given predicate, or <EM>postcondition</EM>. Hence, when we
use &#X201C;calculated&#X201D; or &#X201C;uncalculated&#X201D; here, we refer to the state of
a weakest precondition calculation. A proof obligation (PO) is
calculated if all the weakest precondition calculations and all the
variables instanciations have taken place. Thus, a calculated PO looks
like a regular predicate, while an uncalculated PO still contains the
substitutions expressed in a B component.</P><P>We chose to follow the approach defined in the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A>]:
reducing B substitutions to their smallest syntactic and semantic set
(i.e., generalized substitutions). In the following paragraphs, we
use <EM>GSL</EM> to denote both the syntactic set and the substitutions
that compose it. Following the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A><CITE>, B.3</CITE>], we define the
<EM>GSL</EM> in <EM>BCaml</EM> as an abstract data type, with the following
notable exceptions:
</P><UL CLASS="itemize"><LI CLASS="li-itemize">
The assignment is defined as a multiple substitution; it serves
as a basic construct once the parallel substitutions have been
reduced, and thus can be viewed as an optimisation.</LI><LI CLASS="li-itemize">The repetition substitution "  " is
used to reason about the semantics of the <EM>while substitution</EM>
and to propose a sufficient predicate [<A HREF="draft010.html#BBook"><CITE>2</CITE></A><CITE>, E.7</CITE>] that could
be used instead of the necessary and sufficient predicate.
</LI><LI CLASS="li-itemize">The instanciation ([<I>variable</I>:=<I>expression</I>]<I>substitution</I>) of a
substitution variable (the parameters of an operation, for instance)
is reduced before transforming the substitution. (This idea is not
documented precisely by Abrial [<A HREF="draft010.html#BBook"><CITE>2</CITE></A>], and thus it corresponds
to an extrapolation on our part.)
</LI></UL><P>The first and the third exception are rather straightforward, but the
second requires a more detailed explanation. The real weakest
precondition can be only obtained from a fixpoint application over
this <EM>repetition substitution</EM>. This fixpoint can not be
calculated in general by programming it. Thus, although the repetition
GSL appears in the B-Book, it never appears in the actual calculations
of the proof obligations, so we chose not to include it. Nonetheless,
should the need arise, it could be introduced, because its semantics
have been defined precisely.</P><P>With the help of the abstract data type, proof obligations can be
generated according to the rules described in the B-Book
[<A HREF="draft010.html#BBook"><CITE>2</CITE></A><CITE>, appendix E</CITE>]. The corresponding <EM>BCaml</EM> code was written
with readability in mind, making it easy to match the code with the
rule from which it is derived.</P><H4 CLASS="subsubsection"><A NAME="htoc15"></A>Proof Obligation Generation</H4><P>
<A NAME="sec:POG"></A>
</P><H5 CLASS="paragraph"> </H5><P>The main steps for generating proof obligations from a project can be
divided into precise steps. These steps are described in more detail
below:</P><H5 CLASS="paragraph">Parsing</H5><P>First, the machine and all the machines it depends on are parsed. To
improve efficiency, we decided to directly use the <EM>BCaml</EM> kernel
libraries directly for parsing rather than reading the XML files
produced by the parser.</P><DIV CLASS="minipage">
[language=Bmethod,basicstyle=,frame=single,framesep=1ex]figures/po-timeOut_1_showRlight.amn
</DIV><P>
Uncalculated proof obligation for the
<B>timeOut_1_showRlight</B> operation
<A NAME="fig:po-timeOut_1_showRlight"></A>
</P><H5 CLASS="paragraph">Generation of formulas</H5><P>The formula generation step is based on the B-Book [<A HREF="draft010.html#BBook"><CITE>2</CITE></A><CITE>, appendix
F</CITE>], resulting in proof obligations with the following form:
[<I>Instanciation</I>]<I>Hypothesis</I> &#X21D2; [<I>substitution</I>]<I>Goal</I>.</P><P>This generation method allows more handling flexibility later on, for
instance when debugging the proof obligation generator, or when
showing students how proof obligations are generated, or when the
proof tool applies the substitution to the goal.
Figure <A HREF="#fig:po-timeOut_1_showRlight">4.3</A> shows an example of an
uncalculated proof obligation, derived from the B project presented
in section <A HREF="UML-to-B.html#sec:from-umlocl-models-to-b">7</A>.</P><H5 CLASS="paragraph">Optimizations</H5><P>Several additional optimisations, or processing procedures, can be
applied to the generated formulas. For example, formulas can be
calculated, resulting in predicates that contain no substitutions. It
is also possible to split the goal, by splitting the formula into as
many formulas as there are members of the conjunction in the goal:</P><TABLE CLASS="display dcenter"><TR VALIGN="middle"><TD CLASS="dcell">(<I>H</I> &#X21D2; <I>G</I><SUB>1</SUB> &#X2227; &#X2026; &#X2227; <I>G</I><SUB><I>n</I></SUB>) &#X219D; (<I>H</I> &#X21D2;
<I>G</I><SUB>1</SUB>), &#X2026;, (<I>H</I> &#X21D2; <I>G</I><SUB><I>n</I></SUB>)
</TD></TR>
</TABLE><P>Other possible optimizations that were however not implemented include
removing formulas when the goal is trivially true or appears in the
hypotheses, or changing the form of the formula to adapt it to a
particular theorem prover. Certainly, it is sometimes easier to apply
such transformations to the abstract syntax tree than to XML files
using stylesheets. We did not implement these optimisations because we
do not believe that semantic interpretation is the province of the PO
generator, but rather of the prover, despite the fact that this
semantic interpretation can be easily related to the abstract syntax
(e.g., like a goal appearing in the hypotheses).</P><H5 CLASS="paragraph">Final files and trace information</H5><P>Once the formulas have been generated, some trace information is
embedded into the resulting file. Trace information can be found in
the absolute name of the file, which reflects the kind of proof
obligation that is in the file, and the machine from which it is
generated. This trace information can be used later to find
problematic parts of a B project if the corresponding proof cannot be
achieved. The XML information in the file contains not only the
predicate itself, but also a root tag named (for obvious reasons)
<TT>ProofObligation</TT>. In addition, the file contains a tag that
includes all the free variables of the formula because some theorem
provers require that all variables be bound. This tag helps the
stylesheet to generate a file for such theorem provers more easily.</P><H4 CLASS="subsubsection"><A NAME="htoc16"></A>Exporting to other tools</H4><P>
<A NAME="sec:exporting-to-other-tools"></A>
</P><H5 CLASS="paragraph"> </H5><P>Once the proof obligations in the XML format are available, the XSL
stylesheets allow them to be exported to other tools. For instance,
the proof obligations can be converted into L<sup>A</sup>T<sub>E</sub>Xfiles
(figure <A HREF="#fig:po-timeOut_1_showRlight">4.3</A> is an example of the results
obtained); into text files, which are easily read by humans; into HTML
files, which improve the readability of the formulas; or into a format
suitable for a prover, in order to verify the proof obligations.</P><P>Figure <A HREF="correctness.html#fig:po-phox-timeout1">5.1</A> in section <A HREF="correctness.html#sec:bphox-xsl">5.1</A>
presents the result of an XSL stylesheet application to the proof
obligation shown in figure <A HREF="#fig:po-timeOut_1_showRlight">4.3</A>.
In the next step, the theorem prover is fed
the generated proof obligations file (see
section <A HREF="correctness.html#sec:from-b-proof-to-correctness">5</A>). All of these steps
(including replacing the conjunctions in the hypotheses with
implications) are done via the XSL stylesheet, demonstrating the
<EM>ad hoc</EM> suitability of this technology designed for simple
treatments involving recursion.</P><P>More complex transformations might be doable with stylesheets, but it
would run the risk of becoming uselessly wordy and, more importantly,
less maintainable. For this reason, we advise using stylesheets only
for translations that preserve the overall structure.</P><HR>
<A HREF="case-study.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="correctness.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
</BODY>
</HTML>
