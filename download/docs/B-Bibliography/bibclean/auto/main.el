(TeX-add-style-hook "main"
 (lambda ()
    (LaTeX-add-bibliographies
     "../Bmethod")
    (TeX-run-style-hooks
     "fullpage"
     "pslatex"
     "babel"
     "francais"
     "fontenc"
     "T1"
     "inputenc"
     "latin1"
     "latex2e"
     "art10"
     "article"
     "a4paper"
     "french"
     "citations")))

