#!/bin/sed -f

/[ \t]*\(keywords\)[ \t]*=[ \t]*".*",[ \t]*$/ {

# Transforms semicolons into commas
  s/;/,/g

# Do as for fix-authors.sed: put ; around keywords
  s/\([ \t]*\(keywords\)[ \t]*=[ \t]*"\)/\1;/
  s/\(",[ \t]*$\)/;",/
  s/,/;;/g
# Ooops, put back the trailing comma which has been transformed by the
# previous command
  s/";;[ \t]*$/",/

# Removes capital protection
  s/{\([[:alpha:]-]\+\)}/\1/g

# {\' e} -> e and for similar macros

  s/{\\['`~^"][ \t]*\\\?\([[:alpha:]]\)}/\1/g

# {\xy} -> xy
  s/{\\\([[:alpha:]]\{2\}\)}/\1/g

# Fix spaces inside each entry
# ;   xxx   yyy   ; -> ;xxx   yyy;
  s/;[ \t]*\([^;]\+\)[ \t]*;/;\1;/g
# xxx   yyy -> xxx yyy
  s/[ \t]\+/ /g

# Remove useless capitalisation (not safe for very odd acronyms, though)
# NB: requires GNU sed because of the "lower" command
  s/\<\([[:upper:]]\)\([[:lower:]]\+\)\>/\l\1\2/g

# Removes the points (acronyms do not need points in keywords)
  s/\.//g

# OK, done, we remove the ; and put back commas
  s/";/"/
  s/;",/",/
  s/;;/, /g

}
