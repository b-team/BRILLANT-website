<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
            "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="hevea 1.09">
<LINK rel="stylesheet" type="text/css" href="forms04.css">
<TITLE>From UML models to B specifications</TITLE>
</HEAD>
<BODY >
<A HREF="forms04004.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="forms04006.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
<HR>
<H2 CLASS="section"><A NAME="htoc7">4</A>  From UML models to B specifications</H2><UL>
<LI><A HREF="forms04005.html#toc4">Formalisation of class and state diagrams</A></LI>
</UL>
<P>The basis of our approach is the transformation of the UML model into
a B formal specification. This section presents a systematic
transformation of the UML class diagrams into a B specification. A
major asset of combining UML and B is to give semantics to object
models and to enable the use of automated verification and validation
tools. We use B in order to specify precisely the structure and the
behaviour of the entities composing a system and to prove rigorously
that these satisfy the desired structural and behavioural properties.
These promise increased reliability of software systems, and the
potential of automating the software development process.</P><H3 CLASS="subsection"><A NAME="toc4"></A><A NAME="htoc8">4.1</A>  Formalisation of class and state diagrams</H3><P>The initial B specification (called abstract specification) is
obtained from the UML diagrams and used to check inconsistencies. To
do so, an abstract machine is associated to each class. Subsequently,
the B notation is used to detail each component with the behaviour of
class operations and the global invariants. At this point the
developer must take some important decisions concerning unspecified
properties. She/he introduces these properties in the UML diagrams
using OCL constraints. Then, UML diagrams are translated into B. The
resulting B specification is used to check consistency of the whole
UML diagrams and the OCL constraints.</P><H4 CLASS="subsubsection"><A NAME="htoc9">4.1.1</A>  Assembling the whole B specification</H4><P>Several solutions have been proposed in order to translate
association, inheritance and aggregation. These can be distributed
among the different abstract machines. We adopt a different solution
for associations taking into account the whole class diagram of a
system. In our approach, the specification is composed by a two levels
hierarchy of abstract machines regarding the B inclusion. At the
first level, the root abstract machine represents the system itself.
This machine specifies the whole structure of the system and it
introduces all the associations between classes. Some global
properties and constraints formalizing inheritance and aggregation are
also added. At the second level of the specification, we introduce an
abstract machine representing each class. Each machine is linked to
the root machine by the <SPAN STYLE="font-variant:small-caps">includes</SPAN> link. Fig.<A HREF="#structure">7</A>
shows the structure of the B specification of the railway level
crossing system (Fig.<A HREF="forms04004.html#classdiag1">2</A>).</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04007.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 7: Structure of the B specification</TD></TR>
</TABLE></DIV>
<A NAME="structure"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>This very simple approach makes it easier to define and modify the
whole specification. Since the resulting B specification contains less
B machines and its hierarchical structure is less complex, there is no
problem of knowing where to attach a new class in a hierarchy. Also
proof obligations (OPs) concerning internal properties of a class are
generated independently from its relationships.</P><H4 CLASS="subsubsection"><A NAME="htoc10">4.1.2</A>  Classes.</H4><P> Let us consider the class <I>Barrier</I> and its first B specification, presented in Fig.<A HREF="#class">8</A>. Since a class includes both static and behavioural properties of a set of objects, it seems natural to model it by one abstract machine. The resulting abstract machine Barrier describes the deferred set BARRIER of all the possible instances of the class <I>Barrier</I>. The set of the existing instances is modelled by a variable barrier constrained to be a subset of BARRIER. Each attribute, i.e. <I>bState</I>, is represented by a variable, i.e. bState, defined in the INVARIANT clause as a total function between the set barrier and its associated type, i.e. bSTATE. Each operation of the machine has at least one parameter obj representing the object on which the operation is called. It may have a list of typed arguments args which will be completed in the further translation of state diagrams and OCL constraints. </P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
2
<FONT SIZE=2>
 </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">machine</SPAN></FONT><FONT SIZE=2> Barrier<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">sets</SPAN></FONT><FONT SIZE=2><BR>
   BARRIER;<BR>
  bSTATE={Opened,Closed}<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">variables</SPAN></FONT><FONT SIZE=2><BR>
   barrier, bState<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">invariant</SPAN></FONT><FONT SIZE=2><BR>
   barrier </FONT><FONT SIZE=2>&#X2286;</FONT><FONT SIZE=2> BARRIER </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   bState </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2192;</FONT><FONT SIZE=2> bSTATE<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">operations</SPAN></FONT><FONT SIZE=2><BR>
  openBarrier(obj) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2> obj </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> bState(obj)=Closed </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   bState(obj) := Opened<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2><BR>
  closeBarrier(obj) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2> obj </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> bState(obj)=Opened </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   bState(obj) := Closed<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2><BR>
  st </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2190;</SPAN></FONT><FONT SIZE=2> getState(obj , args) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2> obj </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> args </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> Type</FONT><SUB><FONT SIZE=2><I>args</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> &#X2026;</FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   st := bState(obj) </FONT><FONT SIZE=2>|</FONT><FONT SIZE=2>|</FONT><FONT SIZE=2> &#X2026;<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2>
</FONT>

<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 8: Formalisation of classes (machine Barrier)</TD></TR>
</TABLE></DIV>
<A NAME="class"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><H4 CLASS="subsubsection"><A NAME="htoc11">4.1.3</A>  Associations.</H4><P> Since associations between classes represent couples of instances, they are expressed in B as binary relations between the existing instances of classes. Associations can be expressed more precisely according to the values of the role multiplicities. This is done by constraining the binary relation (&#X2194;) as a function (&#X2192;), partial function (), injection () or bijection () with additional properties on its domain or range. Therefore, the association between the LCC class and the Barrier class <I>lcc_barrier</I> is formalized by a variable lcc_barrier. Since the class diagram of Fig.<A HREF="forms04004.html#classdiag1">2</A> establishes that only one barrier is associated to a LCC system, a typing predicate defining it as a function between the set of level crossings (variable lcc) and the set of barriers (variable barrier) is added to the invariant of the LCC_System machine: lcc_barrier &#X2286; lcc &#X2192; barrier. Fig. <A HREF="#association">9</A> shows the translation of associations. </P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
2
<FONT SIZE=2>
 </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">machine</SPAN></FONT><FONT SIZE=2> LCC_System<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">includes</SPAN></FONT><FONT SIZE=2><BR>
   Barrier, BarrierSensor, Yellow.Light, Red.light, TrainborneCS<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">variables</SPAN></FONT><FONT SIZE=2><BR>
   lcc_barrier,lcc_sensort,lcc_train,<BR>
   redLight,yellowLigh, &#X2026;<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">invariant</SPAN></FONT><FONT SIZE=2><BR>
   lcc_barrier </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2192;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   lcc_sensor </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2192;</FONT><FONT SIZE=2> bSensor </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   lcc_train </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> lcc  train </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   redLight </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2192;</FONT><FONT SIZE=2> Red.light </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   yellowLight </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">&#X2208;</SPAN></FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2192;</FONT><FONT SIZE=2> Yellow.light </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> &#X2026;<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">operations</SPAN></FONT><FONT SIZE=2><BR>
  setLcc_Barrier(obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> , obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   lcc_barrier := lcc_barrier </FONT><FONT SIZE=2>&#X222A;</FONT><FONT SIZE=2> {(obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X21A6;</FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>)}<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2><BR>
  rmvLcc_Barrier(obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> , obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> barrier </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   {(obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X21A6;</FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>)}</FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> lcc_barrier </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   lcc_barrier := lcc_barrier </FONT><FONT SIZE=2>\</FONT><FONT SIZE=2> {(obj</FONT><SUB><FONT SIZE=2><I>i</I></FONT></SUB><FONT SIZE=2> </FONT><FONT SIZE=2>&#X21A6;</FONT><FONT SIZE=2> obj</FONT><SUB><FONT SIZE=2><I>j</I></FONT></SUB><FONT SIZE=2>)}<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2>  &#X2026;</FONT>

<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 9: Formalisation of associations (machine LCC_system)</TD></TR>
</TABLE></DIV>
<A NAME="association"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>When an association is given by a class, then an independent machine
specifying it is created. Aggregations are formalized in the same way
as associations, however these are added directly to the machine
representing the composed class instead to the system machine.
Although inheritance is taken into account by our approach, it is not
relevant in the level crossing case study. Inheritance between two
classes Class<SUB><I>k</I></SUB> and SubClass<SUB><I>ki</I></SUB> is formalized in the System
machine by adding a predicate to the invariant, which constraints the
variable subclass<SUB><I>ki</I></SUB> to be a subset of the variable class<SUB><I>k</I></SUB>:
subclass<SUB><I>ki</I></SUB> &#X2286; class<SUB><I>k</I></SUB>. The semantics of
this invariant is that each object of <I>SubClass</I><SUB><I>k</I></SUB> is also
an object of <I>Class</I><SUB><I>k</I></SUB>. In the invariant of the System
machine it is required that intersection between all subclasses
instances is an empty set, and their union is the superclass set.</P><H4 CLASS="subsubsection"><A NAME="htoc12">4.1.4</A>  Formalisation of state diagrams</H4><P>State diagrams are used to introduce behavoiural properties in the B
specification. The set of all possible states of a class is formalised
by an abstract set which is defined in the respective B machine. An
abstract variable is used to reference the current state of the class
objects. It is defined as a total function, whose domain is the set of
instances and whose range is the set of possible states. Each
transition between two states is formalised by a B operation whose
name is the name of the incoming event. Whereas the precondition of
the operation is deduced from the guard of the transition, the
postcondition describes the transition to the new state. Let us
consider the state diagram of the LCC_System class
(Fig.<A HREF="forms04004.html#statediag0">4</A>). The transition from the showingYlight state to
the closingB state activated by the event timeOut_1 is formalized as
shown in Fig.<A HREF="#transition0">10</A>. Note that we have included here some
information obtained from the OCL definition of the operation
closeBarrier, since this operation is activated by the event
timeOut_1 (we describe the translation of OCL below).</P><P>When the same event may activate two different transitions depending
on a guard condition then both transitions are formalized by the same
operation of the B machine. The <SPAN STYLE="font-variant:small-caps">select</SPAN> close is used to
describe each transition, as illustrated on Fig.<A HREF="#transition0">10</A> for
the formalisation of the event timeOut_2.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
2
<FONT SIZE=2>
 </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">machine</SPAN></FONT><FONT SIZE=2> LCC_System &#X2026;<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">operations</SPAN></FONT><FONT SIZE=2> &#X2026;<BR>
  timeOut_1(obj) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2><BR>
   obj </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   state(obj)=YellowLightOn </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   bStatus(lcc_sensor(obj))=Opened </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   bState(lcc_barrier(obj))=Opened </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   Red.lState(redLight(obj))=Off </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   Yellow.lState(yellowLight(obj))=On<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   state(obj):=ClosingB </FONT><FONT SIZE=2>|</FONT><FONT SIZE=2>|</FONT><FONT SIZE=2><BR>
   closeBarrier(lcc_barrier(obj)) </FONT><FONT SIZE=2>|</FONT><FONT SIZE=2>|</FONT><FONT SIZE=2><BR>
   Yellow.switchOff(yellowLight(obj)) </FONT><FONT SIZE=2>|</FONT><FONT SIZE=2>|</FONT><FONT SIZE=2><BR>
   Red.switchOn(redLight(obj))<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2><BR>
  timeOut_2(obj) =<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">pre</SPAN></FONT><FONT SIZE=2><BR>
   obj </FONT><FONT SIZE=2>&#X2208;</FONT><FONT SIZE=2> lcc </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   state(obj)=ClosingB </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   bState(lcc_barrier(obj))=Closed </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   Red.lState(redLight(obj))=On </FONT><FONT SIZE=2>&#X2227;</FONT><FONT SIZE=2><BR>
   Yellow.lState(yellowLight(obj))=Off<BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
   </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">select</SPAN></FONT><FONT SIZE=2> bStatus(lcc_sensor(obj))=Closed </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
    state(obj):=ClosedB </FONT><FONT SIZE=2>|</FONT><FONT SIZE=2>|</FONT><FONT SIZE=2><BR>
    mode(obj):=Safe<BR>
   </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">when</SPAN></FONT><FONT SIZE=2> bStatus(lcc_sensor(obj))=Opened </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">then</SPAN></FONT><FONT SIZE=2><BR>
    state(obj):=Failure<BR>
   </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">else</SPAN></FONT><FONT SIZE=2> skip </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end</SPAN></FONT><FONT SIZE=2><BR>
  </FONT><FONT SIZE=2><SPAN STYLE="font-variant:small-caps">end ;</SPAN></FONT><FONT SIZE=2><BR>
 </FONT>

<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 10: Formalisation of state diagrams</TD></TR>
</TABLE></DIV>
<A NAME="transition0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>Once class and state diagrams are translated and integrated into the
initial specification, OCL constraints are used to complete the
invariants and operations of the B machines.</P><P>

</P><HR>
<A HREF="forms04004.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="forms04006.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
</BODY>
</HTML>
