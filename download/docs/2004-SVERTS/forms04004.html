<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"
            "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="hevea 1.09">
<LINK rel="stylesheet" type="text/css" href="forms04.css">
<TITLE>From Requirements to UML models</TITLE>
</HEAD>
<BODY >
<A HREF="forms04003.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="forms04005.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
<HR>
<H2 CLASS="section"><A NAME="htoc3">3</A>  From Requirements to UML models</H2><UL>
<LI><A HREF="forms04004.html#toc1">Specification of the RLC system</A></LI>
<LI><A HREF="forms04004.html#toc2">UML-based modelling</A></LI>
<LI><A HREF="forms04004.html#toc3">Adding OCL constraints</A></LI>
</UL>
<P>The &#X201D;explication problem&#X201D;, i.e. the creation of a first valid
description of the required system properties, is a decisive activity
in order to ensure the correctness of the future system. The
consistency of the system relies on the developer's ability to capture
its key safety properties. Therefore, the knowledge of the related
domain plays an important role in the requirements elicitation
activity. Our approach of requirement analysis is based on
[<A HREF="forms04009.html#Levy02a"><CITE>9</CITE></A>] where both static and dynamic properties are taken into
account by different UML diagrams.</P><P>The comprehension of the static properties is obtained by describing
involved entities and their invariant. Here, the Object Constraint
Language is used to express all the properties that cannot be
expressed using only diagrammatic notation, i.e. hypothesis and facts
on subsystems, classes, attributes and associations. Safety conditions
are also described at this step using OCL. The comprehension of the
dynamic behaviour of the future system is obtained by the description
of the manner the actors will interact with it. Two steps are
necessary: first, UML sequence diagrams will be defined in order to
describe both usual and failure scenarios. Second, the complete
expected behaviour of the system will be described as a set of OCL pre
and post-conditions on operations. Putting together all the
operations, UML state diagrams will be defined to describe the global
interaction of a system with each actor of its environment.</P><H3 CLASS="subsection"><A NAME="toc1"></A><A NAME="htoc4">3.1</A>  Specification of the RLC system</H3><P>
A complete description of the traffic control system considered here
is given in [<A HREF="forms04009.html#Jansen00"><CITE>10</CITE></A>]. This description includes also domain
knowledge as a basis for formal specification. The problem is the
specification of a radio-based Railway Level Crossing (RLC)
application that has been developed for the German Railways
[<A HREF="forms04009.html#FFB96"><CITE>11</CITE></A>]. It is distributed over three subsystems: a train-borne
control system (on-board system), a level crossing control system and
an operations centre. The level crossing is situated at a
single-track railway line and a road crossing at the same level. The
intersection area of the road and the railway line is called danger
zone, since trains and road traffic must not enter it at the same
time. Note that this is the main safety constraint that will be taken
into account during the description of the system.</P><H3 CLASS="subsection"><A NAME="toc2"></A><A NAME="htoc5">3.2</A>  UML-based modelling</H3><P>The whole system is composed of three subsystems which communicate
each other. Our study begins by adopting a centric approach regarding
the Level Crossing Control subsystem (called LCC). From this point of
view, the Trainborne Control system (TC) and the Operation Centre (OC)
are actors cooperating and making use of the LCC.</P><P>At this stage, the main entities of interest to be modelled regarding
possible failure conditions of the LCC system have to be identified. A
main cause of failures is the malfunctioning of sensors or actuators.
Defects may occur in the main physical structures, but also control
systems themselves may fail. In the case study only a limited number
of failures are regarded: failures of yellow or red traffic light (to
be regarded separately), the barriers, the vehicle sensor and the
delay or loss of telegrams on the radio network. Consequently, we
consider the following objects interacting with the LCC system, as
shown in Fig.<A HREF="#classdiag1">2</A>: the lights, the barriers, the vehicle
sensors, the trainborne control system and the operations centre. We
consider here only one-side railway line of the level crossing in
order to make more readable the specification of the system.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04002.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 2: Class diagram of the RLC system</TD></TR>
</TABLE></DIV>
<A NAME="classdiag1"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>The traffic lights and barriers at the level crossing are controlled
by the LCC system. The LCC system has to be activated when a train is
approaching the level crossing. In the activated mode a sequence of
actions are performed by the LCC at a specific timing in order to
safely close the crossing and to ensure the danger zone to be free of
road traffic. First, the traffic lights are switched on to show the
yellow light, then after 3 seconds they are switched to red. After
some further 9 seconds the barriers are started to be lowered. The LCC
system signals the safe state of the level crossing if the barriers
have completely been lowered within a maximum time of 6 seconds,
allowing the train to pass the level crossing (Fig.<A HREF="#scenario1">3</A>).</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04003.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 3: Sequence diagram - scenario of train approaching</TD></TR>
</TABLE></DIV>
<A NAME="scenario1"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>The level crossing may be opened again for road traffic when the train
has completely passed the crossing area and the LLC system switches
back to the deactivated mode. The detection of a train approaching at
the level crossing is based on continuous self-localisation of the
train and radio-based communication between the train and the LCC
system. Triggering the vehicle sensor at the rear of the level
crossing will allow the barriers to be opened again and the traffic
lights to be switched off. In the activated mode the LCC system may be
in one of the following substates (Fig.<A HREF="#statediag0">4</A>): showing the
yellow light; closing the barrier; retaining the barrier closed; or
opening the barrier. Note that time expirations occurring after the
activation of the LCC are denoted by the events timeOut_1 (3 seconds
later) and timeOut_2 (9 seconds later).</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04004.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 4: State diagram of the LCC system</TD></TR>
</TABLE></DIV>
<A NAME="statediag0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>When a train is approaching the level crossing, it sets a braking
curve for speed supervision making the train stop at the potential
danger point in failure situation. The LCC system acknowledges receipt
of the activation order to the train. After receipt of the
acknowledgement the TC system waits an appropriate time for the level
crossing to be closed and then sends a status request to the LCC
system. If the level crossing is in its safe state it will be reported
to the train. This will allow the train to cancel the braking curve
and safely pass over the level crossing. This scenario is illustrated
in the sequence diagram of Fig.<A HREF="#scenario0">5</A>.</P><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04005.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 5: Sequence diagram - scenario of train crossing</TD></TR>
</TABLE></DIV>
<A NAME="scenario0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>The sequence diagrams include OCL constraints which are used to define
pre-conditions on operations. These include also time properties, but
their formalisation in B is omitted in this work, since some
extensions to the B language are necessary.</P><H3 CLASS="subsection"><A NAME="toc3"></A><A NAME="htoc6">3.3</A>  Adding OCL constraints</H3><P>Using a standard formal language for constraint specification is an
important step towards formalising complex models, particularly in the
context of safety critical systems. The purpose of OCL (Object
Constraint Language) is to allow constraints on the objects of a
system to be formally specified, preserving the comprehensibility and
readability of the UML models. It facilitates to express the
properties and invariants on the objects and the pre/post-conditions
on the operations. OCL provides a navigation mechanism allowing
attributes, operations and associations to be referenced in the
context of a class or an object (a class variable). It includes query
operators permitting to select and/or modify a set of elements. Each
OCL expression has a specific type and belongs to a specific context.
The context of an OCL expression determines its scope. Only the
visible elements in the context of the expression can be referenced by
means of navigation expressions.</P><P>Safety properties are included in the invariant of the system, in
order to ensure their preservation from the abstract specification
through the implementation. As the main property of the LCC system is
to preserve road traffic and trains to enter the danger zone at the
same time, on a high level of abstraction of the control specification
it is sufficient to model the crossing area and its barrier, as well
as the train which may cross the level crossing at any time. Thus, the
following OCL invariants are specified on these classes as shown in
Fig.<A HREF="#classdiag0">6</A>:
</P><OL CLASS="enumerate" type=1><LI CLASS="li-enumerate">
If there is a train crossing the danger zone then the barrier is closed<BR>
 <FONT COLOR=purple>context CrossingArea inv:<BR>
 not(self.train-&gt;isEmpty()) implies self.barrier.state=Closed </FONT>
</LI><LI CLASS="li-enumerate">If the barrier of the crossing area is opened then no train is approaching the danger zone<BR>
 <FONT COLOR=purple>context Barrier inv:<BR>
 self.state=Opened implies self.guards.train-&gt;isEmpty() </FONT>
</LI><LI CLASS="li-enumerate">The barrier of the crossing area is closed when it is being crossed by a train<BR>
 <FONT COLOR=purple>context PhysicalTrain inv:<BR>
 self.crosses.barrier=Closed </FONT>
</LI></OL><BLOCKQUOTE CLASS="figure"><DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV>
<DIV CLASS="center"><SPAN CLASS="textboxed"><IMG SRC="forms04006.png"></SPAN></DIV>
<DIV CLASS="caption"><TABLE CELLSPACING=6 CELLPADDING=0><TR><TD VALIGN=top ALIGN=left>Figure 6: Constraints on the danger zone</TD></TR>
</TABLE></DIV>
<A NAME="classdiag0"></A>
<DIV CLASS="center"><HR WIDTH="80%" SIZE=2></DIV></BLOCKQUOTE><P>These constraints have to hold true for a more detailed design when
decisions have been made about the actual type of hardware to be used
in an implementation. In the case study, the notion of &#X201C;train passing
the crossing area&#X201D; is used in connection with the activation of the
railway level crossing. Accordingly, the front of a train has to be
detected somehow for accomplishing this task. It is the same for the
rear end of a train. We assume that the train can be detected in a
direct way by introducing abstract vehicle sensors. The detection of
the barrier state is also performed by introducing a barrier sensor.
Therefore the previous OCL invariants can be refined by the following
constraints on the LCC system class of the Fig.<A HREF="#classdiag1">2</A>:
</P><OL CLASS="enumerate" type=1><LI CLASS="li-enumerate">
The red light is switched on whenever the barrier is closed and the yellow light is switched on when the barrier is closing. If both the yellow and the red lights are switched off then the barrier is opened<BR>
 <FONT COLOR=purple>context LCC_System inv:<BR>
 self.theBarrier.state=Closed implies self.redLight.state=On and<BR>
 self.theBarrier.state=Closing implies self.yellowLight.state=On and<BR>
 self.yellowLight.state=Off and self.redLight.state=Off implies
self.theBarrier.state=Opened </FONT>
</LI><LI CLASS="li-enumerate">While there is still a train at the danger zone the level crossing is in the activated state. The activated state is composed by four substates (WaitingAck, Closing, Closed, Opening)<BR>
 <FONT COLOR=purple>context LCC_System inv:<BR>
 not(self.train-&gt;isEmpty()) implies self.state=Activated and<BR>
 </FONT><P><FONT COLOR=purple>Set(Activated)=Set(WaitingAck-&gt;Union(Closing)-&gt;Union(Closed)-&gt;Union(Opening))
</FONT>
</P></LI><LI CLASS="li-enumerate">When the LCC system is in the activated state and the barrier is opened then the level crossing is in unsafe mode<BR>
 <FONT COLOR=purple>context LCC_System inv:<BR>
 self.state=Activated and self.bSensor.state=Opened implies
self.mode=Unsafe </FONT>
</LI><LI CLASS="li-enumerate">If the registered state of the barrier is closed whereas triggering the sensor indicates that it is opened, then the level crossing is in unsafe mode. This is the case when the barrier is in the closing state (the lcc remains unsafe until the barrier has been completly closed)<BR>
 <FONT COLOR=purple>context LCC_System inv:<BR>
 self.bSensor.state=Opened and self.theBarrier.state=Closed)
implies self.mode=Unsafe </FONT>
</LI></OL><P>The operations of the LCC class are specified with OCL pre and
post-conditions. OCL is additionally used in sequence diagrams to
complete preconditions and invariants on operations
(Fig.<A HREF="#scenario1">3</A>). Although state diagrams are used to derive a
first specification of each operation, i.e. describing a state
transition, OCL constraints are needed to add supplementary
information which can not be retrieved from state diagrams.</P><P>Let us consider the closing of the barrier raised by the event timeOut_1. The precondition of the operation closeBarrier has to verify that the yellow light is switched on before sending the closing order to the barrier. It also has to verify than the barrier is not yet closed. The postcondition ensures that the state of the yellow light is off, the state of the red light is on and the state of the barrier is closed. The operation is specified as follows:<BR>
<FONT COLOR=purple>context LCC_System::closeBarrier<BR>
 pre: self.yellowLight.state=On and self.theBarrier.state=Opened<BR>
 post: self.yellowLight.state=Off and self.redLight.state=On and<BR>
 self.theBarrier.state=Closed </FONT>
The precondition of the operation openBarrier which is activated by the trainDetectionRear event verifies that the barrier is closed and the LCC system is in its safe mode. The postcondition ensures the barrier is in the opened state:<BR>
<FONT COLOR=purple>context LCC_System::openBarrier<BR>
 pre: self.theBarrier.state=Closed and self.mode=Safe<BR>
 post: self.theBarrier.state=Opened </FONT></P><P>In the following, we describe first the translation of UML class and
state diagrams into B. Then, we propose transformation rules
translating OCL constraints into B expressions.


</P><HR>
<A HREF="forms04003.html"><IMG SRC="previous_motif.gif" ALT="Previous"></A>
<A HREF="index.html"><IMG SRC="contents_motif.gif" ALT="Up"></A>
<A HREF="forms04005.html"><IMG SRC="next_motif.gif" ALT="Next"></A>
</BODY>
</HTML>
